FROM alpine:3.17

RUN apk add --update --no-cache openssh

RUN mkdir -p ~/.ssh && chmod 700 ~/.ssh
